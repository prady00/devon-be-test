<?php

namespace App\Http\Controllers;

use App\Server;
use Illuminate\Http\Request;

class ServersController extends Controller
{
    public function index(Request $request)
    {

        return Server::paginatedServers($request);

    }

    public function getLocations()
    {
        return Server::distinct()->get(['location']);
    }
}
