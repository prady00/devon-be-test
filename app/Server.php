<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Server extends Model
{
    public static function paginatedServers(Request $request)
    {

        $query = Server::query();

        if(isset($request->minStorage) && isset($request->maxStorage)){
            $query = $query->where('storage','<',$request->maxStorage)
                                    ->where('storage','>',$request->minStorage);
        }

        if(isset($request->ram)){
            $ram = explode(',',$request->ram);
            $query = $query->whereIn('s_ram',$ram);
        }

        if(isset($request->hdd_type)){
            $query = $query->where('HDD', 'like', '%' . $request->hdd_type .'%' );
        }

        if(isset($request->location)){
            $query = $query->where('location',$request->location);
        }

        $data = $query->paginate();

        return $data;

    }
}
