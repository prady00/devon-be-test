<?php

namespace App\Console\Commands;

use App\Server;
use Illuminate\Console\Command;

class ComputeStorage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:computeStorage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Computes storage for faster retrievals';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $servers = Server::all();

        foreach($servers as $server){

            $hdd = $server->HDD;

            $quantity = explode("x", $hdd)[0];

            $storageUnit = '';
            $disks = '';
            $total = 0;

            if (strpos($hdd, 'TB') !== false) {
                $storageUnit = 'TB';
                $disks = $this->getStringBetween($hdd,'x','TB');
                $total = $quantity*$disks*1024;
            }else{
                $storageUnit = 'GB';
                $disks = $this->getStringBetween($hdd,'x','GB');
                $total = $quantity*$disks;
            }

            $server->storage = $total;

            $server->s_ram = explode("GB", $server->RAM)[0] . 'GB';

            $server->save();

        }
    }

    function getStringBetween($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
}
