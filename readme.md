# Devon BE Test

This is a back end for test 

## Cloning and running on local

To clone run following command (it is presumed that npm, node and other common dependencies are already installed on system)

`git clone https://gitlab.com/prady00/devon-be-test.git` 

`cd devon-be-test`

To run, run following command

Create a MySQL DB with name 'devon' and make appropriate changes in .env file

`php artisan migrate`

`php artisan migrate --seed`

`php artisan command:computeStorage`

`php artisan serve`

Navigate to `http://localhost:8000/`

## Documentation for APIs

https://devon6.docs.apiary.io

## Running unit tests

Run `phpunit` to execute the unit tests via [phpunit]

