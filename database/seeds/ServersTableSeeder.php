<?php

use Illuminate\Database\Seeder;

class ServersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('servers')->delete();
        
        \DB::table('servers')->insert(array (
            0 => 
            array (
                'id' => 2,
                'model' => 'Dell R210Intel Xeon X3440',
                'RAM' => '16GBDDR3',
                'HDD' => '2x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '49.99',
            ),
            1 => 
            array (
                'id' => 3,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '119',
            ),
            2 => 
            array (
                'id' => 4,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '131.99',
            ),
            3 => 
            array (
                'id' => 5,
                'model' => 'RH2288v32x Intel Xeon E5-2650V4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '227.99',
            ),
            4 => 
            array (
                'id' => 6,
                'model' => 'RH2288v32x Intel Xeon E5-2620v4',
                'RAM' => '64GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '161.99',
            ),
            5 => 
            array (
                'id' => 7,
                'model' => 'Dell R210-IIIntel Xeon E3-1230v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '72.99',
            ),
            6 => 
            array (
                'id' => 8,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '179.99',
            ),
            7 => 
            array (
                'id' => 9,
                'model' => 'IBM X36302x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '106.99',
            ),
            8 => 
            array (
                'id' => 10,
                'model' => 'HP DL120G7Intel G850',
                'RAM' => '4GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '39.99',
            ),
            9 => 
            array (
                'id' => 11,
                'model' => 'Dell R730XD2x Intel Xeon E5-2667v4',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '364.99',
            ),
            10 => 
            array (
                'id' => 12,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '364.99',
            ),
            11 => 
            array (
                'id' => 13,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '279.99',
            ),
            12 => 
            array (
                'id' => 14,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '286.99',
            ),
            13 => 
            array (
                'id' => 15,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '239.99',
            ),
            14 => 
            array (
                'id' => 16,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '199.99',
            ),
            15 => 
            array (
                'id' => 17,
                'model' => 'Dell R210-IIIntel G530',
                'RAM' => '4GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '35.99',
            ),
            16 => 
            array (
                'id' => 18,
                'model' => 'Dell R210-IIIntel Xeon E3-1220',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '59.99',
            ),
            17 => 
            array (
                'id' => 19,
                'model' => 'Dell R9304x Intel Xeon E7-4850v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '1044.99',
            ),
            18 => 
            array (
                'id' => 20,
                'model' => 'Dell R9304x Intel Xeon E7-4820v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '756.99',
            ),
            19 => 
            array (
                'id' => 21,
                'model' => 'Dell R9304x Intel Xeon E7-4830v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '874.99',
            ),
            20 => 
            array (
                'id' => 22,
                'model' => 'Dell R210-IIIntel Xeon E3-1270v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '89.99',
            ),
            21 => 
            array (
                'id' => 23,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '199.99',
            ),
            22 => 
            array (
                'id' => 24,
                'model' => 'HP DL120G91x Intel E5-1650v3',
                'RAM' => '64GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '154.99',
            ),
            23 => 
            array (
                'id' => 25,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '84.99',
            ),
            24 => 
            array (
                'id' => 26,
                'model' => 'DL20G9Intel Xeon E3-1270v5',
                'RAM' => '16GBDDR4',
                'HDD' => '2x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '112.99',
            ),
            25 => 
            array (
                'id' => 27,
                'model' => 'HP DL120G91x Intel E5-1620v3',
                'RAM' => '32GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '119.99',
            ),
            26 => 
            array (
                'id' => 28,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '154.99',
            ),
            27 => 
            array (
                'id' => 29,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '142.99',
            ),
            28 => 
            array (
                'id' => 30,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '209.99',
            ),
            29 => 
            array (
                'id' => 31,
                'model' => 'RH2288v32x Intel Xeon E5-2650V4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '227.99',
            ),
            30 => 
            array (
                'id' => 32,
                'model' => 'RH2288v32x Intel Xeon E5-2620v4',
                'RAM' => '64GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '161.99',
            ),
            31 => 
            array (
                'id' => 33,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v4',
                'RAM' => '64GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '204.99',
            ),
            32 => 
            array (
                'id' => 34,
                'model' => 'Dell R210-IIIntel Xeon E3-1230v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '72.99',
            ),
            33 => 
            array (
                'id' => 35,
                'model' => 'Dell R7202x Intel Xeon E5-2643',
                'RAM' => '32GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '221.99',
            ),
            34 => 
            array (
                'id' => 36,
                'model' => 'HP DL180G62x Intel Xeon E5645',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '129.99',
            ),
            35 => 
            array (
                'id' => 37,
                'model' => 'Dell R6202x Intel Xeon E5-2650',
                'RAM' => '96GBDDR3',
                'HDD' => '8x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '191.99',
            ),
            36 => 
            array (
                'id' => 38,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '195.99',
            ),
            37 => 
            array (
                'id' => 39,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '295.99',
            ),
            38 => 
            array (
                'id' => 40,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '355.99',
            ),
            39 => 
            array (
                'id' => 41,
                'model' => 'HP DL120G7Intel G850',
                'RAM' => '4GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '163.99',
            ),
            40 => 
            array (
                'id' => 42,
                'model' => 'Dell R210-IIIntel Xeon E3-1230v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '119.99',
            ),
            41 => 
            array (
                'id' => 43,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '272.99',
            ),
            42 => 
            array (
                'id' => 44,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '367.99',
            ),
            43 => 
            array (
                'id' => 45,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '212.99',
            ),
            44 => 
            array (
                'id' => 46,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '277.99',
            ),
            45 => 
            array (
                'id' => 47,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '389.99',
            ),
            46 => 
            array (
                'id' => 48,
                'model' => 'HP DL180G62x Intel Xeon E5645',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '180.99',
            ),
            47 => 
            array (
                'id' => 49,
                'model' => 'Dell R7202x Intel Xeon E5-2643',
                'RAM' => '32GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '246.99',
            ),
            48 => 
            array (
                'id' => 50,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '341.99',
            ),
            49 => 
            array (
                'id' => 51,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '166.99',
            ),
            50 => 
            array (
                'id' => 52,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '304.99',
            ),
            51 => 
            array (
                'id' => 53,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '279.99',
            ),
            52 => 
            array (
                'id' => 54,
                'model' => 'Dell R210-IIIntel G530',
                'RAM' => '4GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '60.99',
            ),
            53 => 
            array (
                'id' => 55,
                'model' => 'Dell R210-IIIntel Xeon E3-1220',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '110.99',
            ),
            54 => 
            array (
                'id' => 56,
                'model' => 'Dell R210Intel Xeon X3440',
                'RAM' => '16GBDDR3',
                'HDD' => '2x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '83.99',
            ),
            55 => 
            array (
                'id' => 57,
                'model' => 'Dell R9304x Intel Xeon E7-4850v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '1069.99',
            ),
            56 => 
            array (
                'id' => 58,
                'model' => 'Dell R9304x Intel Xeon E7-4820v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '781.99',
            ),
            57 => 
            array (
                'id' => 59,
                'model' => 'Dell R9304x Intel Xeon E7-4830v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '899.99',
            ),
            58 => 
            array (
                'id' => 60,
                'model' => 'HP DL120G7Intel G850',
                'RAM' => '4GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '80.99',
            ),
            59 => 
            array (
                'id' => 61,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '224.99',
            ),
            60 => 
            array (
                'id' => 62,
                'model' => 'HP DL120G91x Intel E5-1620v3',
                'RAM' => '32GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '143.99',
            ),
            61 => 
            array (
                'id' => 63,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '252.99',
            ),
            62 => 
            array (
                'id' => 64,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '347.99',
            ),
            63 => 
            array (
                'id' => 65,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '146.99',
            ),
            64 => 
            array (
                'id' => 66,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '257.99',
            ),
            65 => 
            array (
                'id' => 67,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '192.99',
            ),
            66 => 
            array (
                'id' => 68,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '369.99',
            ),
            67 => 
            array (
                'id' => 69,
                'model' => 'Dell R7202x Intel Xeon E5-2643',
                'RAM' => '32GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '226.99',
            ),
            68 => 
            array (
                'id' => 70,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '321.99',
            ),
            69 => 
            array (
                'id' => 71,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '259.99',
            ),
            70 => 
            array (
                'id' => 72,
                'model' => 'Dell R9304x Intel Xeon E7-4830v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '879.99',
            ),
            71 => 
            array (
                'id' => 73,
                'model' => 'Dell R9304x Intel Xeon E7-4850v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '1049.99',
            ),
            72 => 
            array (
                'id' => 74,
                'model' => 'Dell R9304x Intel Xeon E7-4820v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '761.99',
            ),
            73 => 
            array (
                'id' => 75,
                'model' => 'Dell R210-IIIntel G530',
                'RAM' => '4GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '40.99',
            ),
            74 => 
            array (
                'id' => 76,
                'model' => 'Dell R210Intel Xeon X3440',
                'RAM' => '16GBDDR3',
                'HDD' => '2x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '63.99',
            ),
            75 => 
            array (
                'id' => 77,
                'model' => 'Dell R210-IIIntel Xeon E3-1220',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '90.99',
            ),
            76 => 
            array (
                'id' => 78,
                'model' => 'HP DL120G7Intel G850',
                'RAM' => '4GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '60.99',
            ),
            77 => 
            array (
                'id' => 79,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '355.99',
            ),
            78 => 
            array (
                'id' => 80,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '190.99',
            ),
            79 => 
            array (
                'id' => 81,
                'model' => 'HP DL180G62x Intel Xeon E5645',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '160.99',
            ),
            80 => 
            array (
                'id' => 82,
                'model' => 'Dell R210Intel Xeon X3440',
                'RAM' => '16GBDDR3',
                'HDD' => '2x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '197.99',
            ),
            81 => 
            array (
                'id' => 83,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '386.99',
            ),
            82 => 
            array (
                'id' => 84,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '481.99',
            ),
            83 => 
            array (
                'id' => 85,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '280.99',
            ),
            84 => 
            array (
                'id' => 86,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '391.99',
            ),
            85 => 
            array (
                'id' => 87,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '326.99',
            ),
            86 => 
            array (
                'id' => 88,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '503.99',
            ),
            87 => 
            array (
                'id' => 89,
                'model' => 'Dell R7202x Intel Xeon E5-2643',
                'RAM' => '32GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '360.99',
            ),
            88 => 
            array (
                'id' => 90,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '455.99',
            ),
            89 => 
            array (
                'id' => 91,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '393.99',
            ),
            90 => 
            array (
                'id' => 92,
                'model' => 'Dell R9304x Intel Xeon E7-4830v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '1013.99',
            ),
            91 => 
            array (
                'id' => 93,
                'model' => 'Dell R9304x Intel Xeon E7-4850v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '1183.99',
            ),
            92 => 
            array (
                'id' => 94,
                'model' => 'Dell R9304x Intel Xeon E7-4820v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'AmsterdamAMS-01',
                'price' => '895.99',
            ),
            93 => 
            array (
                'id' => 95,
                'model' => 'Dell R210-IIIntel G530',
                'RAM' => '4GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '174.99',
            ),
            94 => 
            array (
                'id' => 96,
                'model' => 'Dell R210-IIIntel Xeon E3-1220',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '224.99',
            ),
            95 => 
            array (
                'id' => 97,
                'model' => 'HP DL120G7Intel G850',
                'RAM' => '4GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '194.99',
            ),
            96 => 
            array (
                'id' => 98,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '489.99',
            ),
            97 => 
            array (
                'id' => 99,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '304.99',
            ),
            98 => 
            array (
                'id' => 100,
                'model' => 'HP DL180G62x Intel Xeon E5645',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '294.99',
            ),
            99 => 
            array (
                'id' => 101,
                'model' => 'Dell R720XD2x Intel Xeon E5-2620',
                'RAM' => '8GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '1907.99',
            ),
            100 => 
            array (
                'id' => 102,
                'model' => 'Dell R720XD2x Intel Xeon E5-2650',
                'RAM' => '8GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '1973.99',
            ),
            101 => 
            array (
                'id' => 103,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '8GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '1907.99',
            ),
            102 => 
            array (
                'id' => 104,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '8GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '1967.99',
            ),
            103 => 
            array (
                'id' => 105,
                'model' => 'HP DL120G7Intel G850',
                'RAM' => '4GBDDR3',
                'HDD' => '4x500GBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '1775.99',
            ),
            104 => 
            array (
                'id' => 106,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '8GBDDR3',
                'HDD' => '4x500GBSATA2',
                'location' => 'AmsterdamAMS-01',
                'price' => '1807.99',
            ),
            105 => 
            array (
                'id' => 107,
                'model' => 'HP DL120G7Intel Xeon E3-1240',
                'RAM' => '16GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$105.99',
            ),
            106 => 
            array (
                'id' => 108,
                'model' => 'Dell R210Intel Xeon X3430',
                'RAM' => '8GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$55.99',
            ),
            107 => 
            array (
                'id' => 109,
                'model' => 'Dell R210-IIIntel Xeon E3-1270v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$121.99',
            ),
            108 => 
            array (
                'id' => 110,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650V4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$565.99',
            ),
            109 => 
            array (
                'id' => 111,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$431.99',
            ),
            110 => 
            array (
                'id' => 112,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$380.99',
            ),
            111 => 
            array (
                'id' => 113,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$360.99',
            ),
            112 => 
            array (
                'id' => 114,
                'model' => 'HP DL180 G92x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$413.99',
            ),
            113 => 
            array (
                'id' => 115,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$305.99',
            ),
            114 => 
            array (
                'id' => 116,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$303.99',
            ),
            115 => 
            array (
                'id' => 117,
                'model' => 'HP DL180 G92x Intel Xeon E5-2630v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$362.99',
            ),
            116 => 
            array (
                'id' => 118,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$360.99',
            ),
            117 => 
            array (
                'id' => 119,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$380.99',
            ),
            118 => 
            array (
                'id' => 120,
                'model' => 'HP DL180 G92x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$413.99',
            ),
            119 => 
            array (
                'id' => 121,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$411.99',
            ),
            120 => 
            array (
                'id' => 122,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$431.99',
            ),
            121 => 
            array (
                'id' => 123,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v4',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$319.99',
            ),
            122 => 
            array (
                'id' => 124,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v4',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$319.99',
            ),
            123 => 
            array (
                'id' => 125,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x300GBSAS',
                'location' => 'DallasDAL-10',
                'price' => '$170.99',
            ),
            124 => 
            array (
                'id' => 126,
                'model' => 'Dell R5102x Intel Xeon E5620',
                'RAM' => '8GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$165.99',
            ),
            125 => 
            array (
                'id' => 127,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$199.99',
            ),
            126 => 
            array (
                'id' => 128,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '16GBDDR3',
                'HDD' => '8x3TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$206.99',
            ),
            127 => 
            array (
                'id' => 129,
                'model' => 'IBM X3650M42x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$220.99',
            ),
            128 => 
            array (
                'id' => 130,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$225.99',
            ),
            129 => 
            array (
                'id' => 131,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$297.99',
            ),
            130 => 
            array (
                'id' => 132,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x300GBSAS',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$170.99',
            ),
            131 => 
            array (
                'id' => 133,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$199.99',
            ),
            132 => 
            array (
                'id' => 134,
                'model' => 'IBM X3650M42x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$220.99',
            ),
            133 => 
            array (
                'id' => 135,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$228.00',
            ),
            134 => 
            array (
                'id' => 136,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$421.99',
            ),
            135 => 
            array (
                'id' => 137,
                'model' => 'DL20G9Intel Xeon E3-1270v5',
                'RAM' => '16GBDDR4',
                'HDD' => '2x1TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$135.99',
            ),
            136 => 
            array (
                'id' => 138,
                'model' => 'Huawei RH1288v22x Intel Xeon E5-2650',
                'RAM' => '8GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$269.99',
            ),
            137 => 
            array (
                'id' => 139,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620V4',
                'RAM' => '64GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$421.99',
            ),
            138 => 
            array (
                'id' => 140,
                'model' => 'Huawei RH2288V22x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$239.99',
            ),
            139 => 
            array (
                'id' => 141,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '395.99',
            ),
            140 => 
            array (
                'id' => 142,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '252.99',
            ),
            141 => 
            array (
                'id' => 143,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '342.99',
            ),
            142 => 
            array (
                'id' => 144,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '364.99',
            ),
            143 => 
            array (
                'id' => 145,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$437.99',
            ),
            144 => 
            array (
                'id' => 146,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$303.99',
            ),
            145 => 
            array (
                'id' => 147,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$411.99',
            ),
            146 => 
            array (
                'id' => 148,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '99',
            ),
            147 => 
            array (
                'id' => 149,
                'model' => 'IBM X36302x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '99',
            ),
            148 => 
            array (
                'id' => 150,
                'model' => 'IBM X3650M42x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '6x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '124.99',
            ),
            149 => 
            array (
                'id' => 151,
                'model' => 'Dell R720XD2x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$225.99',
            ),
            150 => 
            array (
                'id' => 152,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '187.99',
            ),
            151 => 
            array (
                'id' => 153,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '32GBDDR3',
                'HDD' => '8x300GBSAS',
                'location' => 'FrankfurtFRA-10',
                'price' => '176.99',
            ),
            152 => 
            array (
                'id' => 154,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '8GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$183.99',
            ),
            153 => 
            array (
                'id' => 155,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Hong KongHKG-10',
                'price' => 'S$228.00',
            ),
            154 => 
            array (
                'id' => 156,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '8GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$175.99',
            ),
            155 => 
            array (
                'id' => 157,
                'model' => 'Dell R720XD2x Intel Xeon E5-2640v2',
                'RAM' => '64GBDDR3',
                'HDD' => '8x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '249',
            ),
            156 => 
            array (
                'id' => 158,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '316.99',
            ),
            157 => 
            array (
                'id' => 159,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$380.99',
            ),
            158 => 
            array (
                'id' => 160,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x960GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '362.99',
            ),
            159 => 
            array (
                'id' => 161,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x960GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '471.99',
            ),
            160 => 
            array (
                'id' => 162,
                'model' => 'HP DL180G62x Intel Xeon E5645',
                'RAM' => '8GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$187.99',
            ),
            161 => 
            array (
                'id' => 163,
                'model' => 'Dell R720XD2x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$225.99',
            ),
            162 => 
            array (
                'id' => 164,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650V3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$545.99',
            ),
            163 => 
            array (
                'id' => 165,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$303.99',
            ),
            164 => 
            array (
                'id' => 166,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$411.99',
            ),
            165 => 
            array (
                'id' => 167,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '254.99',
            ),
            166 => 
            array (
                'id' => 168,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$305.99',
            ),
            167 => 
            array (
                'id' => 169,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$305.99',
            ),
            168 => 
            array (
                'id' => 170,
                'model' => 'HP DL160 G92x Intel Xeon E5-2630v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$382.99',
            ),
            169 => 
            array (
                'id' => 171,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$437.99',
            ),
            170 => 
            array (
                'id' => 172,
                'model' => 'HP DL180 G92x Intel Xeon E5-2630v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$362.99',
            ),
            171 => 
            array (
                'id' => 173,
                'model' => 'HP DL180 G92x Intel Xeon E5-2630v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '318.99',
            ),
            172 => 
            array (
                'id' => 174,
                'model' => 'Dell R6302x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$555.99',
            ),
            173 => 
            array (
                'id' => 175,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$105.99',
            ),
            174 => 
            array (
                'id' => 176,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$105.99',
            ),
            175 => 
            array (
                'id' => 177,
                'model' => 'HP DL120G7Intel Xeon E3-1270',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$110.99',
            ),
            176 => 
            array (
                'id' => 178,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$105.99',
            ),
            177 => 
            array (
                'id' => 179,
                'model' => 'Dell R210-IIIntel Xeon E3-1270v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$121.99',
            ),
            178 => 
            array (
                'id' => 180,
                'model' => 'HP DL120G6Intel G6950',
                'RAM' => '4GBDDR3',
                'HDD' => '4x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$49.99',
            ),
            179 => 
            array (
                'id' => 181,
                'model' => 'HP DL120G7Intel Xeon E3-1270',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$110.99',
            ),
            180 => 
            array (
                'id' => 182,
                'model' => 'HP DL120G91x Intel E5-1650v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$263.99',
            ),
            181 => 
            array (
                'id' => 183,
                'model' => 'HP DL120G91x Intel E5-1650v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$263.99',
            ),
            182 => 
            array (
                'id' => 184,
                'model' => 'HP DL120G9Intel Xeon E5-1650v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Hong KongHKG-10',
                'price' => 'S$368.99',
            ),
            183 => 
            array (
                'id' => 185,
                'model' => 'HP DL120G9Intel Xeon E5-1650v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$368.99',
            ),
            184 => 
            array (
                'id' => 186,
                'model' => 'Dell R9304x Intel Xeon E7-4820v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$1328.99',
            ),
            185 => 
            array (
                'id' => 187,
                'model' => 'Dell R9304x Intel Xeon E7-4830v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$1516.99',
            ),
            186 => 
            array (
                'id' => 188,
                'model' => 'Dell R9304x Intel Xeon E7-4850v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '1044.99',
            ),
            187 => 
            array (
                'id' => 189,
                'model' => 'Dell R9304x Intel Xeon E7-4850v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$1787.99',
            ),
            188 => 
            array (
                'id' => 190,
                'model' => 'Dell R9304x Intel Xeon E7-4820v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '756.99',
            ),
            189 => 
            array (
                'id' => 191,
                'model' => 'Dell R9304x Intel Xeon E7-4830v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '874.99',
            ),
            190 => 
            array (
                'id' => 192,
                'model' => 'DL20G9Intel Xeon E3-1270v5',
                'RAM' => '16GBDDR4',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$135.99',
            ),
            191 => 
            array (
                'id' => 193,
                'model' => 'HP DL120G91x Intel E5-1620v3',
                'RAM' => '16GBDDR4',
                'HDD' => '4x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$176.99',
            ),
            192 => 
            array (
                'id' => 194,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v4',
                'RAM' => '64GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '204.99',
            ),
            193 => 
            array (
                'id' => 195,
                'model' => 'HP DL120G6Intel G6950',
                'RAM' => '4GBDDR3',
                'HDD' => '4x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$43.99',
            ),
            194 => 
            array (
                'id' => 196,
                'model' => 'HP DL20 G9Intel Xeon E3-1270v5',
                'RAM' => '16GBDDR4',
                'HDD' => '2x1TBSATA2',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$208.00',
            ),
            195 => 
            array (
                'id' => 197,
                'model' => 'HP DL20 G9Intel Xeon E3-1270v5',
                'RAM' => '16GBDDR4',
                'HDD' => '2x1TBSATA2',
                'location' => 'Hong KongHKG-10',
                'price' => 'S$208.00',
            ),
            196 => 
            array (
                'id' => 198,
                'model' => 'Dell R210-IIIntel Xeon E3-1230v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$139.00',
            ),
            197 => 
            array (
                'id' => 199,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$421.99',
            ),
            198 => 
            array (
                'id' => 200,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '350.99',
            ),
            199 => 
            array (
                'id' => 201,
                'model' => 'HP DL120G7Intel Xeon E3-1270',
                'RAM' => '16GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '87.99',
            ),
            200 => 
            array (
                'id' => 202,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$103.99',
            ),
            201 => 
            array (
                'id' => 203,
                'model' => 'Supermicro SC8131x Intel E5-1650v2',
                'RAM' => '64GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$233.99',
            ),
            202 => 
            array (
                'id' => 204,
                'model' => 'HP DL120G91x Intel E5-1650v3',
                'RAM' => '64GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '219.99',
            ),
            203 => 
            array (
                'id' => 205,
                'model' => 'HP DL120G91x Intel E5-1650v3',
                'RAM' => '64GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$275.99',
            ),
            204 => 
            array (
                'id' => 206,
                'model' => 'DL20G9Intel Xeon E3-1270v5',
                'RAM' => '16GBDDR4',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$135.99',
            ),
            205 => 
            array (
                'id' => 207,
                'model' => 'HP DL120G7Intel Xeon E3-1240',
                'RAM' => '16GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$105.99',
            ),
            206 => 
            array (
                'id' => 208,
                'model' => 'HP DL120G91x Intel E5-1650v3',
                'RAM' => '32GBDDR4',
                'HDD' => '4x1TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$239.99',
            ),
            207 => 
            array (
                'id' => 209,
                'model' => 'HP DL120G6Intel Xeon X3440',
                'RAM' => '8GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$69.99',
            ),
            208 => 
            array (
                'id' => 210,
                'model' => 'DL20G9Intel Xeon E3-1270v5',
                'RAM' => '16GBDDR4',
                'HDD' => '2x1TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$135.99',
            ),
            209 => 
            array (
                'id' => 211,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$437.99',
            ),
            210 => 
            array (
                'id' => 212,
                'model' => 'HP DL120G6Intel Xeon X3440',
                'RAM' => '8GBDDR3',
                'HDD' => '2x240GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$69.99',
            ),
            211 => 
            array (
                'id' => 213,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '4x480GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$399.99',
            ),
            212 => 
            array (
                'id' => 214,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '142.99',
            ),
            213 => 
            array (
                'id' => 215,
                'model' => 'Dell R7202x Intel Xeon E5-2643',
                'RAM' => '32GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '221.99',
            ),
            214 => 
            array (
                'id' => 216,
                'model' => 'Dell R720XD2x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '187.99',
            ),
            215 => 
            array (
                'id' => 217,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '165.99',
            ),
            216 => 
            array (
                'id' => 218,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '16GBDDR3',
                'HDD' => '8x3TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '165.99',
            ),
            217 => 
            array (
                'id' => 219,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '247.99',
            ),
            218 => 
            array (
                'id' => 220,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$225.99',
            ),
            219 => 
            array (
                'id' => 221,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$225.99',
            ),
            220 => 
            array (
                'id' => 222,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$297.99',
            ),
            221 => 
            array (
                'id' => 223,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$297.99',
            ),
            222 => 
            array (
                'id' => 224,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$200.99',
            ),
            223 => 
            array (
                'id' => 225,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '16GBDDR3',
                'HDD' => '8x3TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$200.99',
            ),
            224 => 
            array (
                'id' => 226,
                'model' => 'IBM X3650M42x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$220.99',
            ),
            225 => 
            array (
                'id' => 227,
                'model' => 'Dell R5102x Intel Xeon E5504',
                'RAM' => '4GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$104.99',
            ),
            226 => 
            array (
                'id' => 228,
                'model' => 'Dell R5102x Intel Xeon E5620',
                'RAM' => '8GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$165.99',
            ),
            227 => 
            array (
                'id' => 229,
                'model' => 'HP DL120G7Intel G850',
                'RAM' => '4GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$119.99',
            ),
            228 => 
            array (
                'id' => 230,
                'model' => 'Dell R6302x Intel Xeon E5-2630v4',
                'RAM' => '64GBDDR4',
                'HDD' => '2x240GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$489.99',
            ),
            229 => 
            array (
                'id' => 231,
                'model' => 'Dell R6302x Intel Xeon E5-2630v4',
                'RAM' => '64GBDDR4',
                'HDD' => '2x240GBSSD',
                'location' => 'Hong KongHKG-10',
                'price' => 'S$489.99',
            ),
            230 => 
            array (
                'id' => 232,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620V4',
                'RAM' => '64GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'Hong KongHKG-10',
                'price' => 'S$421.99',
            ),
            231 => 
            array (
                'id' => 233,
                'model' => 'RH2288v32x Intel Xeon E5-2620V4',
                'RAM' => '64GBDDR4',
                'HDD' => '4x2TBSATA2',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$319.99',
            ),
            232 => 
            array (
                'id' => 234,
                'model' => 'RH2288v32x Intel Xeon E5-2650V4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$429.99',
            ),
            233 => 
            array (
                'id' => 235,
                'model' => 'Dell R210-IIIntel Xeon E3-1270v2',
                'RAM' => '8GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$199.99',
            ),
            234 => 
            array (
                'id' => 236,
                'model' => 'HP DL120G7Intel G850',
                'RAM' => '16GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '49.99',
            ),
            235 => 
            array (
                'id' => 237,
                'model' => 'Dell R6202x Intel Xeon E5-2620v2',
                'RAM' => '8GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$319.99',
            ),
            236 => 
            array (
                'id' => 238,
                'model' => 'Dell R210-IIIntel Xeon E3-1270v2',
                'RAM' => '8GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Hong KongHKG-10',
                'price' => 'S$199.99',
            ),
            237 => 
            array (
                'id' => 239,
                'model' => 'Dell R210-IIIntel Xeon E3-1230v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '74',
            ),
            238 => 
            array (
                'id' => 240,
                'model' => 'HP DL20G9Intel Xeon E3-1270v5',
                'RAM' => '16GBDDR4',
                'HDD' => '2x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '112.99',
            ),
            239 => 
            array (
                'id' => 241,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '75',
            ),
            240 => 
            array (
                'id' => 242,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '142.99',
            ),
            241 => 
            array (
                'id' => 243,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$235.59',
            ),
            242 => 
            array (
                'id' => 244,
                'model' => 'IBM X3650M42x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$350.59',
            ),
            243 => 
            array (
                'id' => 245,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$355.59',
            ),
            244 => 
            array (
                'id' => 246,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$427.59',
            ),
            245 => 
            array (
                'id' => 247,
                'model' => 'IBM X3650M42x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '295.99',
            ),
            246 => 
            array (
                'id' => 248,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '355.99',
            ),
            247 => 
            array (
                'id' => 249,
                'model' => 'Dell R210-IIIntel Xeon E3-1270v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '103.99',
            ),
            248 => 
            array (
                'id' => 250,
                'model' => 'Dell R210-IIIntel Xeon E3-1270v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$151.99',
            ),
            249 => 
            array (
                'id' => 251,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$135.99',
            ),
            250 => 
            array (
                'id' => 252,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '94.99',
            ),
            251 => 
            array (
                'id' => 253,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$410.99',
            ),
            252 => 
            array (
                'id' => 254,
                'model' => 'HP DL120G7Intel G850',
                'RAM' => '4GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$97.99',
            ),
            253 => 
            array (
                'id' => 255,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$206.99',
            ),
            254 => 
            array (
                'id' => 256,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$451.99',
            ),
            255 => 
            array (
                'id' => 257,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Hong KongHKG-10',
                'price' => 'S$719.99',
            ),
            256 => 
            array (
                'id' => 258,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v4',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$349.99',
            ),
            257 => 
            array (
                'id' => 259,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$461.99',
            ),
            258 => 
            array (
                'id' => 260,
                'model' => 'DL20G9Intel Xeon E3-1270v5',
                'RAM' => '16GBDDR4',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$169.99',
            ),
            259 => 
            array (
                'id' => 261,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$327.99',
            ),
            260 => 
            array (
                'id' => 262,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '277.99',
            ),
            261 => 
            array (
                'id' => 263,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '367.99',
            ),
            262 => 
            array (
                'id' => 264,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '389.99',
            ),
            263 => 
            array (
                'id' => 265,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$333.99',
            ),
            264 => 
            array (
                'id' => 266,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$441.99',
            ),
            265 => 
            array (
                'id' => 267,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$467.99',
            ),
            266 => 
            array (
                'id' => 268,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x300GBSAS',
                'location' => 'DallasDAL-10',
                'price' => '$200.99',
            ),
            267 => 
            array (
                'id' => 269,
                'model' => 'Dell R5102x Intel Xeon E5620',
                'RAM' => '8GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$195.99',
            ),
            268 => 
            array (
                'id' => 270,
                'model' => 'HP DL180G62x Intel Xeon E5645',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$217.99',
            ),
            269 => 
            array (
                'id' => 271,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '16GBDDR3',
                'HDD' => '8x3TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$367.79',
            ),
            270 => 
            array (
                'id' => 272,
                'model' => 'IBM X3650M42x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$250.99',
            ),
            271 => 
            array (
                'id' => 273,
                'model' => 'Dell R720XD2x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$255.99',
            ),
            272 => 
            array (
                'id' => 274,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$327.99',
            ),
            273 => 
            array (
                'id' => 275,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x300GBSAS',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$200.99',
            ),
            274 => 
            array (
                'id' => 276,
                'model' => 'Dell R5102x Intel Xeon E5620',
                'RAM' => '8GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$195.99',
            ),
            275 => 
            array (
                'id' => 277,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '341.99',
            ),
            276 => 
            array (
                'id' => 278,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$410.99',
            ),
            277 => 
            array (
                'id' => 279,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$255.99',
            ),
            278 => 
            array (
                'id' => 280,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$327.99',
            ),
            279 => 
            array (
                'id' => 281,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$255.99',
            ),
            280 => 
            array (
                'id' => 282,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$327.99',
            ),
            281 => 
            array (
                'id' => 283,
                'model' => 'Dell R720XD2x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$255.99',
            ),
            282 => 
            array (
                'id' => 284,
                'model' => 'IBM X36302x Intel Xeon E5620',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$202.99',
            ),
            283 => 
            array (
                'id' => 285,
                'model' => 'IBM X3650M42x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$250.99',
            ),
            284 => 
            array (
                'id' => 286,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$255.99',
            ),
            285 => 
            array (
                'id' => 287,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '16GBDDR3',
                'HDD' => '8x3TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$367.79',
            ),
            286 => 
            array (
                'id' => 288,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '16GBDDR3',
                'HDD' => '8x3TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$200.99',
            ),
            287 => 
            array (
                'id' => 289,
                'model' => 'IBM X3650M42x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$250.99',
            ),
            288 => 
            array (
                'id' => 290,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$333.99',
            ),
            289 => 
            array (
                'id' => 291,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$441.99',
            ),
            290 => 
            array (
                'id' => 292,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '279.99',
            ),
            291 => 
            array (
                'id' => 293,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$335.99',
            ),
            292 => 
            array (
                'id' => 294,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$335.99',
            ),
            293 => 
            array (
                'id' => 295,
                'model' => 'HP DL160 G92x Intel Xeon E5-2630v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$412.99',
            ),
            294 => 
            array (
                'id' => 296,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$467.99',
            ),
            295 => 
            array (
                'id' => 297,
                'model' => 'HP DL180 G92x Intel Xeon E5-2630v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '343.99',
            ),
            296 => 
            array (
                'id' => 298,
                'model' => 'HP DL180 G92x Intel Xeon E5-2630v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$412.99',
            ),
            297 => 
            array (
                'id' => 299,
                'model' => 'HP DL120G7Intel Xeon E3-1270',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '99.99',
            ),
            298 => 
            array (
                'id' => 300,
                'model' => 'HP DL120G6Intel G6950',
                'RAM' => '4GBDDR3',
                'HDD' => '4x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$79.99',
            ),
            299 => 
            array (
                'id' => 301,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$135.99',
            ),
            300 => 
            array (
                'id' => 302,
                'model' => 'HP DL120G7Intel Xeon E3-1270',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$140.99',
            ),
            301 => 
            array (
                'id' => 303,
                'model' => 'Supermicro SC813MTQIntel Xeon E5-1650v2',
                'RAM' => '64GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$233.99',
            ),
            302 => 
            array (
                'id' => 304,
                'model' => 'Supermicro SC813MTQIntel Xeon E5-1650v2',
                'RAM' => '64GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$233.99',
            ),
            303 => 
            array (
                'id' => 305,
                'model' => 'HP DL120G6Intel Xeon X3440',
                'RAM' => '8GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$99.99',
            ),
            304 => 
            array (
                'id' => 306,
                'model' => 'Dell R210-IIIntel Xeon E3-1270v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$151.99',
            ),
            305 => 
            array (
                'id' => 307,
                'model' => 'Supermicro SC813MTQIntel Xeon E5-1650v2',
                'RAM' => '64GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$233.99',
            ),
            306 => 
            array (
                'id' => 308,
                'model' => 'HP DL120G7Intel Xeon E3-1270',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$140.99',
            ),
            307 => 
            array (
                'id' => 309,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$135.99',
            ),
            308 => 
            array (
                'id' => 310,
                'model' => 'Dell R9304x Intel Xeon E7-4820v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$1953.99',
            ),
            309 => 
            array (
                'id' => 311,
                'model' => 'Dell R9304x Intel Xeon E7-4830v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$2141.99',
            ),
            310 => 
            array (
                'id' => 312,
                'model' => 'Dell R9304x Intel Xeon E7-4850v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '1069.99',
            ),
            311 => 
            array (
                'id' => 313,
                'model' => 'Dell R9304x Intel Xeon E7-4850v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$2412.99',
            ),
            312 => 
            array (
                'id' => 314,
                'model' => 'Dell R9304x Intel Xeon E7-4820v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '781.99',
            ),
            313 => 
            array (
                'id' => 315,
                'model' => 'Dell R9304x Intel Xeon E7-4830v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '899.99',
            ),
            314 => 
            array (
                'id' => 316,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$451.99',
            ),
            315 => 
            array (
                'id' => 317,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '375.99',
            ),
            316 => 
            array (
                'id' => 318,
                'model' => 'Dell R210-IIIntel Xeon E3-1230v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '96.99',
            ),
            317 => 
            array (
                'id' => 319,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '272.99',
            ),
            318 => 
            array (
                'id' => 320,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x300GBSAS',
                'location' => 'FrankfurtFRA-10',
                'price' => '166.99',
            ),
            319 => 
            array (
                'id' => 321,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '16GBDDR3',
                'HDD' => '8x3TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '190.99',
            ),
            320 => 
            array (
                'id' => 322,
                'model' => 'Dell R720XD2x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '212.99',
            ),
            321 => 
            array (
                'id' => 323,
                'model' => 'Dell R7202x Intel Xeon E5-2643',
                'RAM' => '32GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '246.99',
            ),
            322 => 
            array (
                'id' => 324,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '190.99',
            ),
            323 => 
            array (
                'id' => 325,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$230.99',
            ),
            324 => 
            array (
                'id' => 326,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$719.99',
            ),
            325 => 
            array (
                'id' => 327,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '8GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$143.99',
            ),
            326 => 
            array (
                'id' => 328,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$111.99',
            ),
            327 => 
            array (
                'id' => 329,
                'model' => 'Dell R210Intel Xeon X3430',
                'RAM' => '8GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$61.99',
            ),
            328 => 
            array (
                'id' => 330,
                'model' => 'Dell R210-IIIntel Xeon E3-1270v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$127.99',
            ),
            329 => 
            array (
                'id' => 331,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$111.99',
            ),
            330 => 
            array (
                'id' => 332,
                'model' => 'HP DL120G7Intel Xeon E3-1270',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$116.99',
            ),
            331 => 
            array (
                'id' => 333,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$386.99',
            ),
            332 => 
            array (
                'id' => 334,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x300GBSAS',
                'location' => 'DallasDAL-10',
                'price' => '$176.99',
            ),
            333 => 
            array (
                'id' => 335,
                'model' => 'Dell R5102x Intel Xeon E5620',
                'RAM' => '8GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$171.99',
            ),
            334 => 
            array (
                'id' => 336,
                'model' => 'HP DL180G62x Intel Xeon E5645',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$193.99',
            ),
            335 => 
            array (
                'id' => 337,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$230.99',
            ),
            336 => 
            array (
                'id' => 338,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '16GBDDR3',
                'HDD' => '8x3TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$230.99',
            ),
            337 => 
            array (
                'id' => 339,
                'model' => 'IBM X3650M42x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$226.99',
            ),
            338 => 
            array (
                'id' => 340,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$231.99',
            ),
            339 => 
            array (
                'id' => 341,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$303.99',
            ),
            340 => 
            array (
                'id' => 342,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$427.99',
            ),
            341 => 
            array (
                'id' => 343,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Hong KongHKG-10',
                'price' => 'S$569.99',
            ),
            342 => 
            array (
                'id' => 344,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '146.99',
            ),
            343 => 
            array (
                'id' => 345,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v4',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$464.99',
            ),
            344 => 
            array (
                'id' => 346,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$576.99',
            ),
            345 => 
            array (
                'id' => 347,
                'model' => 'DL20G9Intel Xeon E3-1270v5',
                'RAM' => '16GBDDR4',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$280.99',
            ),
            346 => 
            array (
                'id' => 348,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '257.99',
            ),
            347 => 
            array (
                'id' => 349,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '347.99',
            ),
            348 => 
            array (
                'id' => 350,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$309.99',
            ),
            349 => 
            array (
                'id' => 351,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$417.99',
            ),
            350 => 
            array (
                'id' => 352,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$443.99',
            ),
            351 => 
            array (
                'id' => 353,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '369.99',
            ),
            352 => 
            array (
                'id' => 354,
                'model' => 'Dell R720XD2x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$231.99',
            ),
            353 => 
            array (
                'id' => 355,
                'model' => 'Dell R720XD2x Intel Xeon E5-2640v2',
                'RAM' => '64GBDDR3',
                'HDD' => '8x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '279',
            ),
            354 => 
            array (
                'id' => 356,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '321.99',
            ),
            355 => 
            array (
                'id' => 357,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$386.99',
            ),
            356 => 
            array (
                'id' => 358,
                'model' => 'Dell R720XD2x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$231.99',
            ),
            357 => 
            array (
                'id' => 359,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$569.99',
            ),
            358 => 
            array (
                'id' => 360,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$309.99',
            ),
            359 => 
            array (
                'id' => 361,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$417.99',
            ),
            360 => 
            array (
                'id' => 362,
                'model' => 'HP DL160 G92x Intel Xeon E5-2630v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '323.99',
            ),
            361 => 
            array (
                'id' => 363,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '259.99',
            ),
            362 => 
            array (
                'id' => 364,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$311.99',
            ),
            363 => 
            array (
                'id' => 365,
                'model' => 'HP DL160 G92x Intel Xeon E5-2630v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$388.99',
            ),
            364 => 
            array (
                'id' => 366,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$311.99',
            ),
            365 => 
            array (
                'id' => 367,
                'model' => 'HP DL160 G92x Intel Xeon E5-2630v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$388.99',
            ),
            366 => 
            array (
                'id' => 368,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$443.99',
            ),
            367 => 
            array (
                'id' => 369,
                'model' => 'HP DL120G7Intel Xeon E3-1270',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '96.99',
            ),
            368 => 
            array (
                'id' => 370,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$111.99',
            ),
            369 => 
            array (
                'id' => 371,
                'model' => 'HP DL120G7Intel Xeon E3-1270',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$116.99',
            ),
            370 => 
            array (
                'id' => 372,
                'model' => 'Supermicro SC813MTQIntel Xeon E5-1650v2',
                'RAM' => '64GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$209.99',
            ),
            371 => 
            array (
                'id' => 373,
                'model' => 'HP DL120G6Intel Xeon X3440',
                'RAM' => '8GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$75.99',
            ),
            372 => 
            array (
                'id' => 374,
                'model' => 'Dell R210-IIIntel Xeon E3-1270v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$127.99',
            ),
            373 => 
            array (
                'id' => 375,
                'model' => 'Supermicro SC813MTQIntel Xeon E5-1650v2',
                'RAM' => '64GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$209.99',
            ),
            374 => 
            array (
                'id' => 376,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '92.99',
            ),
            375 => 
            array (
                'id' => 377,
                'model' => 'HP DL120G6Intel G6950',
                'RAM' => '4GBDDR3',
                'HDD' => '4x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$55.99',
            ),
            376 => 
            array (
                'id' => 378,
                'model' => 'Supermicro SC813MTQIntel Xeon E5-1650v2',
                'RAM' => '64GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$209.99',
            ),
            377 => 
            array (
                'id' => 379,
                'model' => 'Dell R9304x Intel Xeon E7-4820v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$1553.99',
            ),
            378 => 
            array (
                'id' => 380,
                'model' => 'Dell R9304x Intel Xeon E7-4830v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$1741.99',
            ),
            379 => 
            array (
                'id' => 381,
                'model' => 'Dell R9304x Intel Xeon E7-4850v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '1049.99',
            ),
            380 => 
            array (
                'id' => 382,
                'model' => 'Dell R9304x Intel Xeon E7-4850v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$2012.99',
            ),
            381 => 
            array (
                'id' => 383,
                'model' => 'Dell R9304x Intel Xeon E7-4820v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '761.99',
            ),
            382 => 
            array (
                'id' => 384,
                'model' => 'Dell R9304x Intel Xeon E7-4830v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '879.99',
            ),
            383 => 
            array (
                'id' => 385,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$427.99',
            ),
            384 => 
            array (
                'id' => 386,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '355.99',
            ),
            385 => 
            array (
                'id' => 387,
                'model' => 'Dell R7202x Intel Xeon E5-2643',
                'RAM' => '32GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '226.99',
            ),
            386 => 
            array (
                'id' => 388,
                'model' => 'Dell R720XD2x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '192.99',
            ),
            387 => 
            array (
                'id' => 389,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '252.99',
            ),
            388 => 
            array (
                'id' => 390,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '16GBDDR3',
                'HDD' => '8x3TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '170.99',
            ),
            389 => 
            array (
                'id' => 391,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '170.99',
            ),
            390 => 
            array (
                'id' => 392,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$200.99',
            ),
            391 => 
            array (
                'id' => 393,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '16GBDDR3',
                'HDD' => '8x3TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$200.99',
            ),
            392 => 
            array (
                'id' => 394,
                'model' => 'IBM X3650M42x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$226.99',
            ),
            393 => 
            array (
                'id' => 395,
                'model' => 'Dell R5102x Intel Xeon E5504',
                'RAM' => '4GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$110.99',
            ),
            394 => 
            array (
                'id' => 396,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x300GBSAS',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$176.99',
            ),
            395 => 
            array (
                'id' => 397,
                'model' => 'HP DL180G62x Intel Xeon E5645',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$193.99',
            ),
            396 => 
            array (
                'id' => 398,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$200.99',
            ),
            397 => 
            array (
                'id' => 399,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '16GBDDR3',
                'HDD' => '8x3TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$230.99',
            ),
            398 => 
            array (
                'id' => 400,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$231.99',
            ),
            399 => 
            array (
                'id' => 401,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$231.99',
            ),
            400 => 
            array (
                'id' => 402,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$303.99',
            ),
            401 => 
            array (
                'id' => 403,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$303.99',
            ),
            402 => 
            array (
                'id' => 404,
                'model' => 'IBM X3650M42x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$226.99',
            ),
            403 => 
            array (
                'id' => 405,
                'model' => 'Dell R5102x Intel Xeon E5620',
                'RAM' => '8GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$171.99',
            ),
            404 => 
            array (
                'id' => 406,
                'model' => 'HP DL120G6Intel Xeon X3440',
                'RAM' => '4GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$236.79',
            ),
            405 => 
            array (
                'id' => 407,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$272.79',
            ),
            406 => 
            array (
                'id' => 408,
                'model' => 'HP DL120G7Intel Xeon E3-1270',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$277.79',
            ),
            407 => 
            array (
                'id' => 409,
                'model' => 'Dell R210-IIIntel Xeon E3-1270v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$288.79',
            ),
            408 => 
            array (
                'id' => 410,
                'model' => 'Dell R210Intel Xeon X3440',
                'RAM' => '16GBDDR3',
                'HDD' => '2x2TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$237.79',
            ),
            409 => 
            array (
                'id' => 411,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$272.79',
            ),
            410 => 
            array (
                'id' => 412,
                'model' => 'Dell R210Intel Xeon X3430',
                'RAM' => '8GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$222.79',
            ),
            411 => 
            array (
                'id' => 413,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '226.99',
            ),
            412 => 
            array (
                'id' => 414,
                'model' => 'HP DL120G7Intel Xeon E3-1270',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '230.99',
            ),
            413 => 
            array (
                'id' => 415,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$547.79',
            ),
            414 => 
            array (
                'id' => 416,
                'model' => 'Dell R5102x Intel Xeon E5620',
                'RAM' => '8GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$332.79',
            ),
            415 => 
            array (
                'id' => 417,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x300GBSAS',
                'location' => 'DallasDAL-10',
                'price' => '$337.79',
            ),
            416 => 
            array (
                'id' => 418,
                'model' => 'HP DL180G62x Intel Xeon E5645',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$354.79',
            ),
            417 => 
            array (
                'id' => 419,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '64GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$367.79',
            ),
            418 => 
            array (
                'id' => 420,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '16GBDDR3',
                'HDD' => '8x3TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$367.79',
            ),
            419 => 
            array (
                'id' => 421,
                'model' => 'IBM X3650M42x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$387.79',
            ),
            420 => 
            array (
                'id' => 422,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$392.79',
            ),
            421 => 
            array (
                'id' => 423,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$464.79',
            ),
            422 => 
            array (
                'id' => 424,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$588.79',
            ),
            423 => 
            array (
                'id' => 425,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v4',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$994.99',
            ),
            424 => 
            array (
                'id' => 426,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$1106.99',
            ),
            425 => 
            array (
                'id' => 427,
                'model' => 'DL20G9Intel Xeon E3-1270v5',
                'RAM' => '16GBDDR4',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$810.99',
            ),
            426 => 
            array (
                'id' => 428,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$1055.99',
            ),
            427 => 
            array (
                'id' => 429,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '391.99',
            ),
            428 => 
            array (
                'id' => 430,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '481.99',
            ),
            429 => 
            array (
                'id' => 431,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$578.79',
            ),
            430 => 
            array (
                'id' => 432,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$470.79',
            ),
            431 => 
            array (
                'id' => 433,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '503.99',
            ),
            432 => 
            array (
                'id' => 434,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$604.79',
            ),
            433 => 
            array (
                'id' => 435,
                'model' => 'Dell R720XD2x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$392.79',
            ),
            434 => 
            array (
                'id' => 436,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '455.99',
            ),
            435 => 
            array (
                'id' => 437,
                'model' => 'Dell R730XD2x Intel Xeon E5-2630v4',
                'RAM' => '128GBDDR4',
                'HDD' => '4x480GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$547.79',
            ),
            436 => 
            array (
                'id' => 438,
                'model' => 'IBM X36302x Intel Xeon E5620',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$339.79',
            ),
            437 => 
            array (
                'id' => 439,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$392.79',
            ),
            438 => 
            array (
                'id' => 440,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$464.79',
            ),
            439 => 
            array (
                'id' => 441,
                'model' => 'Dell R720XD2x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$392.79',
            ),
            440 => 
            array (
                'id' => 442,
                'model' => 'Dell R730XD2x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$470.79',
            ),
            441 => 
            array (
                'id' => 443,
                'model' => 'Dell R730XD2x Intel Xeon E5-2650v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$578.79',
            ),
            442 => 
            array (
                'id' => 444,
                'model' => 'HP DL160 G92x Intel Xeon E5-2630v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '457.99',
            ),
            443 => 
            array (
                'id' => 445,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '393.99',
            ),
            444 => 
            array (
                'id' => 446,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$472.79',
            ),
            445 => 
            array (
                'id' => 447,
                'model' => 'HP DL180 G92x Intel Xeon E5-2620v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$472.79',
            ),
            446 => 
            array (
                'id' => 448,
                'model' => 'HP DL160 G92x Intel Xeon E5-2630v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$549.79',
            ),
            447 => 
            array (
                'id' => 449,
                'model' => 'Dell R730XD2x Intel Xeon E5-2670v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$604.79',
            ),
            448 => 
            array (
                'id' => 450,
                'model' => 'HP DL180 G92x Intel Xeon E5-2630v3',
                'RAM' => '128GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$549.79',
            ),
            449 => 
            array (
                'id' => 451,
                'model' => 'Supermicro SC813MTQIntel Xeon E5-1650v2',
                'RAM' => '64GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$370.79',
            ),
            450 => 
            array (
                'id' => 452,
                'model' => 'Supermicro SC813MTQIntel Xeon E5-1650v2',
                'RAM' => '64GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'San FranciscoSFO-12',
                'price' => '$370.79',
            ),
            451 => 
            array (
                'id' => 453,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '16GBDDR3',
                'HDD' => '2x500GBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$272.79',
            ),
            452 => 
            array (
                'id' => 454,
                'model' => 'HP DL120G7Intel Xeon E3-1270',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$277.79',
            ),
            453 => 
            array (
                'id' => 455,
                'model' => 'Dell R210-IIIntel Xeon E3-1270v2',
                'RAM' => '16GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$288.79',
            ),
            454 => 
            array (
                'id' => 456,
                'model' => 'Supermicro SC813MTQIntel Xeon E5-1650v2',
                'RAM' => '64GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'DallasDAL-10',
                'price' => '$370.79',
            ),
            455 => 
            array (
                'id' => 457,
                'model' => 'HP DL120G6Intel G6950',
                'RAM' => '4GBDDR3',
                'HDD' => '4x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$216.79',
            ),
            456 => 
            array (
                'id' => 458,
                'model' => 'Dell R9304x Intel Xeon E7-4820v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$4203.99',
            ),
            457 => 
            array (
                'id' => 459,
                'model' => 'Dell R9304x Intel Xeon E7-4830v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$4391.99',
            ),
            458 => 
            array (
                'id' => 460,
                'model' => 'Dell R9304x Intel Xeon E7-4850v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '1183.99',
            ),
            459 => 
            array (
                'id' => 461,
                'model' => 'Dell R9304x Intel Xeon E7-4850v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'SingaporeSIN-11',
                'price' => 'S$4662.99',
            ),
            460 => 
            array (
                'id' => 462,
                'model' => 'Dell R9304x Intel Xeon E7-4820v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '895.99',
            ),
            461 => 
            array (
                'id' => 463,
                'model' => 'Dell R9304x Intel Xeon E7-4830v3',
                'RAM' => '64GBDDR4',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '1013.99',
            ),
            462 => 
            array (
                'id' => 464,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$588.79',
            ),
            463 => 
            array (
                'id' => 465,
                'model' => 'Supermicro SC846Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '24x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '489.99',
            ),
            464 => 
            array (
                'id' => 466,
                'model' => 'Dell R7202x Intel Xeon E5-2643',
                'RAM' => '32GBDDR3',
                'HDD' => '2x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '360.99',
            ),
            465 => 
            array (
                'id' => 467,
                'model' => 'Dell R720XD2x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '326.99',
            ),
            466 => 
            array (
                'id' => 468,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x300GBSAS',
                'location' => 'FrankfurtFRA-10',
                'price' => '280.99',
            ),
            467 => 
            array (
                'id' => 469,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '16GBDDR3',
                'HDD' => '8x3TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '304.99',
            ),
            468 => 
            array (
                'id' => 470,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'FrankfurtFRA-10',
                'price' => '386.99',
            ),
            469 => 
            array (
                'id' => 471,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '16GBDDR3',
                'HDD' => '8x3TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$367.79',
            ),
            470 => 
            array (
                'id' => 472,
                'model' => 'IBM X3650M42x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'San FranciscoSFO-12',
                'price' => '$387.79',
            ),
            471 => 
            array (
                'id' => 473,
                'model' => 'Dell R5102x Intel Xeon E5504',
                'RAM' => '4GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$271.79',
            ),
            472 => 
            array (
                'id' => 474,
                'model' => 'Dell R5102x Intel Xeon E5620',
                'RAM' => '8GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$332.79',
            ),
            473 => 
            array (
                'id' => 475,
                'model' => 'HP DL180G62x Intel Xeon E5620',
                'RAM' => '32GBDDR3',
                'HDD' => '8x300GBSAS',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$337.79',
            ),
            474 => 
            array (
                'id' => 476,
                'model' => 'HP DL180G62x Intel Xeon E5645',
                'RAM' => '32GBDDR3',
                'HDD' => '8x2TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$354.79',
            ),
            475 => 
            array (
                'id' => 477,
                'model' => 'HP DL380eG82x Intel Xeon E5-2420',
                'RAM' => '16GBDDR3',
                'HDD' => '8x3TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$367.79',
            ),
            476 => 
            array (
                'id' => 478,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$392.79',
            ),
            477 => 
            array (
                'id' => 479,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '128GBDDR3',
                'HDD' => '1x120GBSSD',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$464.79',
            ),
            478 => 
            array (
                'id' => 480,
                'model' => 'IBM X3650M42x Intel Xeon E5-2620',
                'RAM' => '32GBDDR3',
                'HDD' => '2x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$387.79',
            ),
            479 => 
            array (
                'id' => 481,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '8GBDDR3',
                'HDD' => '4x500GBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$2169.99',
            ),
            480 => 
            array (
                'id' => 482,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '8GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$2289.99',
            ),
            481 => 
            array (
                'id' => 483,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '8GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$2361.99',
            ),
            482 => 
            array (
                'id' => 484,
                'model' => 'HP DL120G7Intel Xeon E3-1230',
                'RAM' => '8GBDDR3',
                'HDD' => '4x500GBSATA2',
                'location' => 'DallasDAL-10',
                'price' => '$2169.99',
            ),
            483 => 
            array (
                'id' => 485,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '8GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'FrankfurtFRA-10',
                'price' => '1967.99',
            ),
            484 => 
            array (
                'id' => 486,
                'model' => 'HP DL380pG82x Intel Xeon E5-2620',
                'RAM' => '8GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$2289.99',
            ),
            485 => 
            array (
                'id' => 487,
                'model' => 'HP DL380pG82x Intel Xeon E5-2650',
                'RAM' => '8GBDDR3',
                'HDD' => '4x1TBSATA2',
                'location' => 'Washington D.C.WDC-01',
                'price' => '$2361.99',
            ),
        ));
        
        
    }
}